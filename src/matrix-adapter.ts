/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.*
 */

'use strict';

import { Adapter, Device } from 'gateway-addon';
import MatrixWrapper from './matrix-wrapper';

const notifyDescription = {
  '@type': 'NotificationAction',
  title: 'post',
  description: 'Send a Matrix message',
  input: {
    type: 'object' as 'object', // Added "as" to satisfy TypeScript
    required: [
      'message',
    ],
    properties: {
      isNotice: {
        title: 'Is this message a notice?',
        type: 'boolean',
        default: false,
      },
      message: {
        title: 'Message',
        type: 'string',
      },
      room: {
        title: 'Room',
        type: 'string',
        description: 'Internal room ID or alias',
        default: '',
        pattern: '^[!#].+:.+$',
      },
    },
  },
};

class MatrixDevice extends Device {
  private config: any;
  private messages: { [key: string]: {
    isNotice?: boolean,
    message: string,
    room?: string,
  }};

  constructor(adapter: Adapter, manifest: any, private matrixWrapper: MatrixWrapper) {
    super(adapter, manifest.display_name);
    this['@context'] = 'https://iot.mozilla.org/schemas/';
    this.setTitle(manifest.display_name);
    this.setDescription(manifest.description);
    this.config = manifest.moziot.config;

    if (this.config.default_room_id) {
      notifyDescription.input.properties.room.default = this.config.default_room_id;
    } else {
      // If default_room_id is not defined, it's required when posting.
      notifyDescription.input.required.push('room');
    }
    this.addAction(notifyDescription.title, notifyDescription);

    this.messages = {};

    if (this.config.messages) {
      for (const message of this.config.messages) {
        this.messages[message.name] = {
          isNotice: message.is_notice,
          message: message.message,
          room: message.room_id,
        };

        const action = {
          '@type': notifyDescription['@type'],
          title: message.name,
          description: notifyDescription.description
        };

        console.log(`Creating action for ${message.name}`);
        this.addAction(message.name, action);
      }
    }
  }

  async performAction(action: any) {
    action.start();

    if (action.name === notifyDescription.title) {
      await this.send(action.input);
    } else {
      const message = this.messages[action.name];

      if (message) {
        await this.send(message);
      } else {
        console.warn(`Unknown action ${action}`);
      }
    }

    action.finish();
  }

  async send({ isNotice, message, room }: { isNotice?: boolean, message?: string, room?: string}) {
    room = room || this.config.default_room_id;
    if (typeof message !== 'string') throw new Error('The message needs to be a string.');
    if (typeof room !== 'string') throw new Error('The room needs to be a string.');

    // console.log(`Sending post (room: ${roomAliasOrId}): ${message}`);
    await this.matrixWrapper.sendText({
      isNotice,
      message,
      roomAliasOrId: room,
    });
  }
}

export class MatrixAdapter extends Adapter {
  constructor(addonManager: any, manifest: any, matrixWrapper: MatrixWrapper) {
    super(addonManager, MatrixAdapter.name, manifest.name);

    addonManager.addAdapter(this);
    const device = new MatrixDevice(this, manifest, matrixWrapper);
    this.handleDeviceAdded(device);
  }
}
