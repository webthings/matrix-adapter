# Changelog

## 0.4.3 (2020-10-15)
* Fix broken build

## 0.4.2 (2021-10-15)
* Upgrade dependencies

## 0.4.1 (2021-07-02)
* Fix predefined messages getting posted to the default room.

## 0.4.0 (2021-07-02)
* Updated to support the WebThings gateway 1.0
* Dropped support for previous gateway versions

## 0.3.2 (2020-07-09)
* Nothing new

## 0.3.1 (2020-07-09)
* Fix broken build

## 0.3.0 (2020-07-08)
* Resolve room aliases
* Auto-join upgraded rooms

## 0.2.1 (2020-06-27)
* Nothing new

## 0.2.0 (2020-06-26)
* Posting now takes three properties: a message, a roomId and isNotice.
* Throw error when required config fields are missing.
* The room ID in the config became optional. The Notifier does not appear if no room ID got configured for it.
* A default roomID for messages can be specified.
* Added support for Notification Levels to the notifier.

## 0.1.0 (2020-06-19)
* Initial release
* Restricted to one Matrix room
* Added notifier
